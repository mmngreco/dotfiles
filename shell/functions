#!/usr/bin/env bash
toggleEtsIndex() {

    if [ -z ${ETS_INDEX+x} ]; then
        echo now pip will use pypi.
        export ETS_INDEX=0
        unset PIP_INDEX_URL
        unset PIP_TRUSTED_HOST
    else
        echo now pip will use etspip.
        export ETS_INDEX=1
        export PIP_INDEX_URL=$ETS_PIP_INDEX_URL
        export PIP_TRUSTED_HOST=$ETS_PIP_TRUSTED_HOST
    fi

}

dotfiles() {
    case $1 in
        h)
            echo "Commands: go, commit, source, pull, link, *";;
        help)
            echo "Commands: go, commit, source, pull, link, *";;
        install)
            dotfilesInstall ${@:2} ;;
        i)
            dotfilesInstall ${@:2} ;;
        cd)
            goDotfiles ;;
        go)
            goDotfiles ;;
        cm)
            dotfilesCommit ;;
        commit)
            dotfilesCommit ;;
        ca)
            dotfilesCommit all ;;
        cs)
            dotfilesCommit secret ;;
        source)
            dotfilesSource ;;
        pull)
            dotfilesPull ;;
        link)
            dotfilesLink ;;
        e)
            dotfiles edit ;;
        edit)
            pushd $DOTFILES > /dev/null
            nvim .
            popd > /dev/null
            ;;
        ec)
            dotfiles edit
            dotfiles commit
            ;;
    esac
}

goDotfiles() {
    pushd $DOTFILES > /dev/null
}

dotfilesCommit() {
    pushd $DOTFILES > /dev/null
    case $1 in
        all)
            dotfilesCommit && \
            dotfilesCommit secret
            ;;
        secret)
            echo -e "${COLOR_RED}Commiting secret dotfiles.${COLOR_NC}"
            git secret hide && \
            git add . && \
            git commit -m "auto commit secrets" && \
            git push && \
            git secret reveal -f -p $GIT_SECRET_PASS
            ;;
        *)
            echo -e "${COLOR_RED}Commiting dotfiles.${COLOR_NC}"
            git add . && \
            git commit -m "auto commit" && \
            git push
            ;;
    esac
    popd > /dev/null
}

dotfilesPull () {

    pushd $DOTFILES > /dev/null
    git fetch
    git pull
    git secret reveal -p $GIT_SECRET_PASS
    popd > /dev/null

}

dotfilesSource() {
    if [[ -z "$DOTFILES" ]]; then
        export DOTFILES="${1:-$HOME/.dotfiles}"
    fi
    source $DOTFILES/shell/common
}

dotfilesLink() {
    echo Linking dotfiles...
    pushd $DOTFILES > /dev/null && ./install -Q ; popd > /dev/null
    # clear
}


dotfilesInstall() {
    if [[ -z "$@" ]]
    then
        echo Here is the list of available scripts:
        find ~/.dotfiles/software/ \
            | fzf --select-1 --height 50% --multi --bind "space:toggle" \
            | xargs -L1 -I FILES bash -c "FILES"
    else
        echo "$@"
        for pkg in "$@"
        do
            echo Installing $pkg
            source $HOME/.dotfiles/software/$pkg
        done
    fi
}


addToMyGitProjects(){
    if [[ "$MYGITPROJECTS" != *"$1"*  ]]; then
        # only if exists the folder it will be exported
        [ -d "$1" ] && export MYGITPROJECTS="$MYGITPROJECTS:$1"
    fi
}

addToPath(){
    if [[ "$PATH" != *"$1"*  ]]; then
        export PATH="$PATH:$1"
    fi
}

addToPathFront(){
    if [[ "$PATH" != *"$1"*  ]]; then
        export PATH="$1:$PATH"
    fi
}


recordingScreen() {
    tmux new-session -s recording -d rec_screen
    clear
}

recordingStop() {
    tmux send-keys -t recording:1 C-c
    nautilus $HOME/Videos &
    clear
}


set_rand_wallpaper(){
    find ~/github/mmngreco/wallpapers \
        -type f \
        -not -name "README.md" \
        -not -path "*/.git/*" \
        | shuf -n1\
        | xargs -L1 -I IMG hsetroot -fill IMG
}



pip-lastest() {
    pip install $1==kk |& grep -oP '.*\b\K\d+\.\d+\.\d+\b'
}


ipytdd ()  {
    ipython
    echo "Restarting ipython, press a key to abort."
    read -t 1 cancel
    if [ -z "$cancel" ]
    then
        ipytdd
    fi
}


conda-here () {
    # conda-here [prefix] [*conda-args]
    local pyver="python=3.7"
    local prefix="${1:-$PWD/.venv/$(echo $pyver | tr -d = | tr -d .)}"

    if [ $# -eq 0 ]; then
        conda create --prefix $prefix $pyver pip -y
    else
        conda create --prefix $prefix ${@:2}
    fi

    conda activate "$(abspath $prefix)"

    if [ ! -f ./$AUTOENV_ENV_FILENAME ]; then
        echo conda activate \""$(abspath $prefix)"\" >> $AUTOENV_ENV_FILENAME
        echo $AUTOENV_ENV_FILENAME >> .gitignore
    fi
}


conda-activate() {
    local prefix="$PWD/.venv"
    local selected=$(find $prefix -maxdepth 1 -mindepth 1 -type d | fzf --select-1 --ansi -q "$1" --height 10%)
    conda activate $selected
}

# =============================================================================
# Docker

dfix() {
    # see https://serverfault.com/a/642984/573706
    # apt-get install bridge-utils
    sudo pkill docker
    sudo iptables -t nat -F
    sudo ifconfig docker0 down
    sudo brctl delbr docker0
    sudo service docker restart
}


drun() {

    DIR=$PWD
    IMAGE_NAME=${1:-mmngreco/dev:0.3.2}
    echo Container name:
    echo $CONTAINER_NAME

    docker run \
        --env "DOTFILES=/root/.dotfiles" \
        --volume "$HOME/.ssh:/root/.ssh" \
        --volume "$DIR:/root/$(basename $DIR)" \
        --volume "$HOME/.dotfiles:/root/.dotfiles" \
        --volume "$HOME/.dotfiles/shell/bashrc:/root/.bashrc" \
        --workdir "/root/$(basename $DIR)" \
        --name "$CONTAINER_NAME" \
        --interactive \
        --tty \
        "$IMAGE_NAME" \
        $2
}


ddev() {

    DIR=$PWD
    IMAGE_NAME=${1:-$DEFAULT_IMG}
    SUFFIX="${2:-dev}"
    CONTAINER_NAME="$(basename $DIR)-$(echo $SUFFIX | tr -d .)"

    if [ "$(docker ps -a -q -f name=$CONTAINER_NAME)" ]
    then
        echo "Attaching the container $CONTAINER_NAME"
        docker stop $CONTAINER_NAME
        docker start -ia $CONTAINER_NAME
    else
        echo "Creating the container..."
        docker run \
            --env TERM="xterm-256color" \
            --volume "$HOME/.ssh:/root/.ssh" \
            --volume "$DIR:/git/$(basename $DIR)" \
            --workdir "/git/$(basename $DIR)" \
            --name "$CONTAINER_NAME" \
            --interactive \
            --tty \
            "$IMAGE_NAME"
    fi

}


dclean() {
    docker system prune -a -f
    sudo systemctl stop docker
    sudo systemctl stop docker.socket
    sudo rm -rf /var/lib/docker
    sudo systemctl start docker.socket
    sudo systemctl start docker

}

chown-here () {
    sudo chown -R $USERNAME:$USERNAME ./
}

selectProjectFzf () {
    # Print out the abslute path of every project (which has .git) at HOME dir.
    # query="${1:-tesser}"
    query="${1}"
    listProjects | fzf --select-1 --ansi -q "$query" --height 10% -m --bind "space:toggle"
}

fzfProjects () {
    # Print out the abslute path of every project (which has .git) at HOME dir.
    # query="${1:-tesser}"
    selectProjectFzf $@
}

nvp () {
    # neovim project
    query="${1}"
    fzfProjects $query | xargs -I DIR bash -c "pushd DIR > /dev/null && nvim DIR && popd > /dev/null"
}


txw() {

    if [[ -z "$@" ]]
    then
        # no arguments
        folder_query="${1}"
        selectProjectFzf $folder_query \
            | xargs -L1 -I FOLDER bash -c "tmux new-window -c FOLDER"
    else
        # when arguments are passed
        for folder_query in $@
        do
            selectProjectFzf $folder_query \
                | xargs -L1 -I FOLDER bash -c "tmux new-window -c FOLDER"
        done
    fi
}


txs() {
    case $1 in
        com)
            tmux new-session -d -s com -c ~/gitlab/COM
            tmux send-keys -t com "txw $2 && clear" Enter
            tmux attach -t com
            ;;
        vpn)
            tmux new-session -d -s vpn
            tmux send-keys -t vpn "ets-vpn" Enter
            ;;
        gitlab)
            tmux new-session -d -s "gitlab" -c ~/gitlab
            tmux send-keys -t gitlab "txw $2 && clear" Enter
            tmux attach -t gitlab
            ;;
        github)
            tmux new-session -d -s "github" -c ~/github
            tmux send-keys "txw $2 && clear" Enter
            tmux attach -t github
            ;;
        cancel)
            ;;
        *)
            tmux-sessionizer $1
            ;;
    esac
}


# =============================================================================
# cheat sheet
cs () {
    # cheat sheet
    url="$(echo cht.sh/$@ | tr ' ' '+')"
    echo $url
    curl -s $url
}

csl () {
    # cheat sheet pipe less
    cs $@ | less
}

function extract {
  if [ -z "$1" ]; then
    echo "Usage: extract <path/file_name>.<zip|rar|bz2|gz|tar|tbz2|tgz|Z|7z|xz|ex|tar.bz2|tar.gz|tar.xz>"
  else
    if [ -f $1 ]; then
      case $1 in
        *.tar.bz2)   tar xvjf $1    ;;
        *.tar.gz)    tar xvzf $1    ;;
        *.tar.xz)    tar xvJf $1    ;;
        *.txz)       tar xvJf $1    ;;
        *.lzma)      unlzma $1      ;;
        *.bz2)       bunzip2 $1     ;;
        *.rar)       unrar x -ad $1 ;;
        *.gz)        gunzip $1      ;;
        *.tar)       tar xvf $1     ;;
        *.tbz2)      tar xvjf $1    ;;
        *.tgz)       tar xvzf $1    ;;
        *.zip)       unzip $1       ;;
        *.Z)         uncompress $1  ;;
        *.7z)        7z x $1        ;;
        *.xz)        unxz $1        ;;
        *.exe)       cabextract $1  ;;
        *)           echo "extract: '$1' - unknown archive method" ;;
      esac
    else
      echo "$1 - file does not exist"
    fi
  fi
}

function extract_and_remove {
  extract $1
  rm -f $1
}

function abspath() {
    if [ -d "$1" ]; then
        echo "$(cd $1; pwd)"
    elif [ -f "$1" ]; then
        if [[ $1 == */* ]]; then
            echo "$(cd ${1%/*}; pwd)/${1##*/}"
        else
            echo "$(pwd)/$1"
        fi
    fi
}


function gi() { curl -L -s https://www.gitignore.io/api/$@ ;}


# nq() { d=$(basename "$PWD"); nd=$(printf "../ex%02d*/" $((${d:2:2}+1))); cd $nd ; }

# to go to directory of previous question
# pq() { d=$(basename "$PWD"); pd=$(printf "../ex%02d*/" $((${d:2:2}-1))); cd $pd ; }

# =============================================================================
# Git

is_in_git_repo() {
  git rev-parse HEAD > /dev/null 2>&1
}

_gf() {
  # Selects git file
  is_in_git_repo &&
    git -c color.status=always status --short |
    fzf --height 40% -m --ansi --nth 2..,.. | awk '{print $2}'
}

_gb() {
  # Selects git branch
  is_in_git_repo &&
    git branch -a -vv --color=always | grep -v '/HEAD\s' |
    fzf --height 40% --ansi --multi --tac | sed 's/^..//' | awk '{print $1}' |
    sed 's#^remotes/[^/]*/##'
}

_gt() {
  # Selects git tag
  is_in_git_repo &&
    git tag --sort -version:refname |
    fzf --height 40% --multi
}

_gh() {
  # Select git commit from the history
  is_in_git_repo &&
    git log --date=short --format="%C(green)%C(bold)%cd %C(auto)%h%d %s (%an)" --graph |
    fzf --height 40% --ansi --no-sort --reverse --multi | grep -o '[a-f0-9]\{7,\}'
}

_gr() {
  # Select git remotes
  is_in_git_repo &&
    git remote -v | awk '{print $1 " " $2}' | uniq |
    fzf --height 40% --tac | awk '{print $1}'
}

parse_git_branch () {
    git name-rev HEAD 2> /dev/null | sed 's#HEAD\ \(.*\)# [git:\1]#'
}

parse_svn_branch() {
    parse_svn_url | sed -e 's#^'"$(parse_svn_repository_root)"'##g' | awk -F / '{print " [svn:" $2 "]"}'
}

parse_svn_url() {
    svn info 2>/dev/null | sed -ne 's#^URL: ##p'
}

parse_svn_repository_root() {
    svn info 2>/dev/null | grep -e '^Repository Root:*' | sed -e 's#^Repository Root: *\(.*\)#\1\/#g '
    #svn info 2>/dev/null | sed -ne 's#^Repository Root: ##p'
}

pyright-config-here() {
    cp $DOTFILES/python/pyrightconfig.json .
}

pip-dev-tools() {
    pip install -r $DOTFILES/python/requirements-dev.txt
}
