" ====  pydocstring
nmap <C-d> <Plug>(pydocstring)
let g:pydocstring_doq_path="$PYTHON_NEOVIM/../doq"
let g:pydocstring_templates_dir="$HOME/.config/nvim/plugged/vim-pydocstring/test/templates/numpy"
