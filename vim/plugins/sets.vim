" https://github.com/awesome-streamers/awesome-streamerrc/blob/master/ThePrimeagen/init.vim
" set clipboard=unnamedplus
set scrollback=10000
set guicursor=
set relativenumber
set nohlsearch
set hidden
set noerrorbells
set tabstop=4 softtabstop=4
set shiftwidth=4
set expandtab
set smartindent
set number
set nowrap
set noswapfile
set nobackup
set undodir=$HOME/.vim/undodir
set undofile
set incsearch
set termguicolors
set scrolloff=8
" set noshowmode
set signcolumn=yes
set isfname+=@-@

" Give more space for displaying messages.
set cmdheight=1
" Having longer updatetime (default is 4000 ms = 4 s) leads to noticeable
" delays and poor user experience.
set timeoutlen=220
set updatetime=50

set colorcolumn=80

" Don't pass messages to |ins-completion-menu|.
set shortmess+=c

" ==== column limits
set textwidth=79

set formatprg="par -rTbgqR -B=.,?_A_a -Q=_s>\| -w" . textwidth
