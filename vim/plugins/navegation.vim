" nnoremap <C-k> :cnext<CR>
" nnoremap <C-j> :cprev<CR>
" movements
nnoremap <leader>h :wincmd h<CR>
nnoremap <leader>j :wincmd j<CR>
nnoremap <leader>k :wincmd k<CR>
nnoremap <leader>l :wincmd l<CR>
